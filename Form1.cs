﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Web;

namespace TestApp
{
    public partial class Form1 : Form
    {
        private bool IS_STOPPED = false;

        public Form1()
        {
            InitializeComponent();
        }

        System.Windows.Forms.Timer tmr = null;
        DateTime startTime;
        TimeSpan lastTime;

        private void StartTimer()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 100;
            tmr.Tick += new EventHandler(TimerTick);
            startTime = DateTime.Now;
            lastTime = TimeSpan.Zero;
            tmr.Enabled = true;
        }

        private void ContinueTimer()
        {
            tmr.Start();
            startTime = DateTime.Now;
        }

        private void StopTimer()
        {
            tmr.Stop();
            lastTime += DateTime.Now - startTime;
        }

        void TimerTick(object sender, EventArgs e)
        {
            var currTime = (DateTime.Now - startTime) + lastTime;
            TimeWorking.Text = currTime.Hours + " ч " + currTime.Minutes + " мин " + currTime.Seconds + " сек";
        }

        CancellationTokenSource cts;

        private bool CheckFile(FileInfo file, CancellationToken ct)
        {
            var text = @TextToSearch.Text;
            var searchWindow = new StringBuilder(text.Length);
            var stream = new StreamReader(file.OpenRead());

            bool eof = false;
            for (int i = 0; i < text.Length; ++i)
            {
                ct.ThrowIfCancellationRequested();
                while (IS_STOPPED) ;
                if (stream.EndOfStream)
                {
                    eof = true;
                    break;
                }
                searchWindow.Append((char)stream.Read());
            }
            if (eof)
                return false;
            if (searchWindow.ToString() == text)
                return true;

            while (!stream.EndOfStream)
            {
                ct.ThrowIfCancellationRequested();
                while (IS_STOPPED) ;
                searchWindow.Remove(0, 1);
                searchWindow.Append((char)stream.Read());
                if (searchWindow.ToString() == text)
                {
                    return true;
                }
            }

            return false;
        }

        private async void StartButton_Click(object sender, EventArgs e)
        {
            cts = new CancellationTokenSource();
            StartDirectory.Enabled = false;
            NamePattern.Enabled = false;
            TextToSearch.Enabled = false;
            StartButton.Enabled = false;
            StopButton.Enabled = true;
            FilesFound.Nodes.Clear();
            StartTimer();

            // добавить проверку
            var dirInfo = new DirectoryInfo(@StartDirectory.Text);
            var candidates = dirInfo.GetFiles(@NamePattern.Text, SearchOption.AllDirectories);
            var filesCount = 0;
            FilesFound.Nodes.Add(dirInfo.Name, dirInfo.Name);

            foreach (var file in candidates)
            {
                CurrentFile.Text = file.Name;
                FilesCount.Text = filesCount.ToString();
                ++filesCount;

                try
                {
                    var found = await Task.Run(() => CheckFile(file, cts.Token));
                    /* Добавить файл с учётом иерархии */
                    if (found)
                    {
                        /*if (!FilesFound.Nodes.ContainsKey(file.Directory.Name))
                        {
                            var dir = file.Directory;
                            TreeNode prevNode = null;
                            while (!FilesFound.Nodes.ContainsKey(dir.Name))
                            {
                                var currNode = new TreeNode() { Name = dir.Name, Text = dir.Name };
                                if (prevNode != null)
                                    currNode.Nodes.Add(prevNode);
                                prevNode = currNode;
                                dir = dir.Parent;
                            }
                            FilesFound.Nodes[dir.Name].Nodes.Add(prevNode);
                        }
                        FilesFound.
                        FilesFound.Nodes[file.Directory.Name].Nodes.Add(file.Name, file.Name);*/

                        List<string> path = new List<string>();
                        path.Add(file.Name);
                        var dir = file.Directory;
                        while (dir.Name != dirInfo.Name)
                        {
                            path.Add(dir.Name);
                            dir = dir.Parent;
                        }

                        var node = FilesFound.Nodes[dirInfo.Name];
                        foreach (var d in path.Reverse<string>())
                        {
                            if (!node.Nodes.ContainsKey(d))
                                node = node.Nodes.Add(d, d);
                            else
                                node = node.Nodes[d];
                        }

                    }
                }
                catch (OperationCanceledException oce)
                {
                    break;
                }
            }

            StartDirectory.Enabled = true;
            NamePattern.Enabled = true;
            TextToSearch.Enabled = true;
            StartButton.Enabled = true;
            StopButton.Enabled = false;
            tmr.Enabled = false;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.StartDirectory = StartDirectory.Text;
            Properties.Settings.Default.NamePattern = NamePattern.Text;
            Properties.Settings.Default.TextToSearch = TextToSearch.Text;
            Properties.Settings.Default.Save();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StartDirectory.Text = Properties.Settings.Default.StartDirectory;
            NamePattern.Text = Properties.Settings.Default.NamePattern;
            TextToSearch.Text = Properties.Settings.Default.TextToSearch;
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            ContinueTimer();
            IS_STOPPED = false;
            ContinueButton.Enabled = false;
            StartNewButton.Enabled = false;
            StopButton.Enabled = true;
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            StopTimer();
            IS_STOPPED = true;
            StopButton.Enabled = false;
            ContinueButton.Enabled = true;
            StartNewButton.Enabled = true;
        }

        private void StartNewButton_Click(object sender, EventArgs e)
        {
            if (cts != null)
            {
                cts.Cancel();
            }

            IS_STOPPED = false;
            StartNewButton.Enabled = false;
            ContinueButton.Enabled = false;
            StartButton.Enabled = true;
            StartDirectory.Enabled = true;
            NamePattern.Enabled = true;
            TextToSearch.Enabled = true;
            tmr.Enabled = false;
            TimeWorking.Text = CurrentFile.Text = FilesCount.Text = "";
        }
    }
}
