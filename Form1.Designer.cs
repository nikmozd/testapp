﻿namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartDirectoryLabel = new System.Windows.Forms.Label();
            this.StartDirectory = new System.Windows.Forms.TextBox();
            this.NamePatternLabel = new System.Windows.Forms.Label();
            this.NamePattern = new System.Windows.Forms.TextBox();
            this.TextToSearchLabel = new System.Windows.Forms.Label();
            this.TextToSearch = new System.Windows.Forms.TextBox();
            this.TimeWorkingLabel = new System.Windows.Forms.Label();
            this.CurrentFileLabel = new System.Windows.Forms.Label();
            this.FilesCountLabel = new System.Windows.Forms.Label();
            this.FilesFound = new System.Windows.Forms.TreeView();
            this.FilesFoundLabel = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.TimeWorking = new System.Windows.Forms.Label();
            this.CurrentFile = new System.Windows.Forms.Label();
            this.FilesCount = new System.Windows.Forms.Label();
            this.StartNewButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // StartDirectoryLabel
            // 
            this.StartDirectoryLabel.AutoSize = true;
            this.StartDirectoryLabel.Location = new System.Drawing.Point(573, 36);
            this.StartDirectoryLabel.Name = "StartDirectoryLabel";
            this.StartDirectoryLabel.Size = new System.Drawing.Size(122, 13);
            this.StartDirectoryLabel.TabIndex = 0;
            this.StartDirectoryLabel.Text = "Стартовая директория";
            // 
            // StartDirectory
            // 
            this.StartDirectory.Location = new System.Drawing.Point(576, 52);
            this.StartDirectory.Name = "StartDirectory";
            this.StartDirectory.Size = new System.Drawing.Size(197, 20);
            this.StartDirectory.TabIndex = 1;
            // 
            // NamePatternLabel
            // 
            this.NamePatternLabel.AutoSize = true;
            this.NamePatternLabel.Location = new System.Drawing.Point(573, 92);
            this.NamePatternLabel.Name = "NamePatternLabel";
            this.NamePatternLabel.Size = new System.Drawing.Size(116, 13);
            this.NamePatternLabel.TabIndex = 2;
            this.NamePatternLabel.Text = "Шаблон имени файла";
            // 
            // NamePattern
            // 
            this.NamePattern.Location = new System.Drawing.Point(576, 108);
            this.NamePattern.Name = "NamePattern";
            this.NamePattern.Size = new System.Drawing.Size(197, 20);
            this.NamePattern.TabIndex = 3;
            // 
            // TextToSearchLabel
            // 
            this.TextToSearchLabel.AutoSize = true;
            this.TextToSearchLabel.Location = new System.Drawing.Point(573, 151);
            this.TextToSearchLabel.Name = "TextToSearchLabel";
            this.TextToSearchLabel.Size = new System.Drawing.Size(86, 13);
            this.TextToSearchLabel.TabIndex = 4;
            this.TextToSearchLabel.Text = "Искомый текст";
            // 
            // TextToSearch
            // 
            this.TextToSearch.Location = new System.Drawing.Point(576, 167);
            this.TextToSearch.Multiline = true;
            this.TextToSearch.Name = "TextToSearch";
            this.TextToSearch.Size = new System.Drawing.Size(197, 106);
            this.TextToSearch.TabIndex = 5;
            // 
            // TimeWorkingLabel
            // 
            this.TimeWorkingLabel.AutoSize = true;
            this.TimeWorkingLabel.Location = new System.Drawing.Point(308, 305);
            this.TimeWorkingLabel.Name = "TimeWorkingLabel";
            this.TimeWorkingLabel.Size = new System.Drawing.Size(100, 13);
            this.TimeWorkingLabel.TabIndex = 6;
            this.TimeWorkingLabel.Text = "Прошло времени: ";
            // 
            // CurrentFileLabel
            // 
            this.CurrentFileLabel.AutoSize = true;
            this.CurrentFileLabel.Location = new System.Drawing.Point(308, 341);
            this.CurrentFileLabel.Name = "CurrentFileLabel";
            this.CurrentFileLabel.Size = new System.Drawing.Size(87, 13);
            this.CurrentFileLabel.TabIndex = 7;
            this.CurrentFileLabel.Text = "Текущий файл: ";
            // 
            // FilesCountLabel
            // 
            this.FilesCountLabel.AutoSize = true;
            this.FilesCountLabel.Location = new System.Drawing.Point(308, 374);
            this.FilesCountLabel.Name = "FilesCountLabel";
            this.FilesCountLabel.Size = new System.Drawing.Size(115, 13);
            this.FilesCountLabel.TabIndex = 8;
            this.FilesCountLabel.Text = "Обработано файлов: ";
            // 
            // FilesFound
            // 
            this.FilesFound.Location = new System.Drawing.Point(12, 52);
            this.FilesFound.Name = "FilesFound";
            this.FilesFound.Size = new System.Drawing.Size(249, 358);
            this.FilesFound.TabIndex = 9;
            // 
            // FilesFoundLabel
            // 
            this.FilesFoundLabel.AutoSize = true;
            this.FilesFoundLabel.Location = new System.Drawing.Point(77, 25);
            this.FilesFoundLabel.Name = "FilesFoundLabel";
            this.FilesFoundLabel.Size = new System.Drawing.Size(102, 13);
            this.FilesFoundLabel.TabIndex = 10;
            this.FilesFoundLabel.Text = "Найденные файлы";
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(354, 79);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(123, 23);
            this.StartButton.TabIndex = 11;
            this.StartButton.Text = "Начать поиск";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ContinueButton
            // 
            this.ContinueButton.Enabled = false;
            this.ContinueButton.Location = new System.Drawing.Point(354, 120);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(123, 23);
            this.ContinueButton.TabIndex = 12;
            this.ContinueButton.Text = "Продолжить поиск";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Enabled = false;
            this.StopButton.Location = new System.Drawing.Point(354, 163);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(123, 23);
            this.StopButton.TabIndex = 13;
            this.StopButton.Text = "Прервать поиск";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // TimeWorking
            // 
            this.TimeWorking.AutoSize = true;
            this.TimeWorking.Location = new System.Drawing.Point(446, 305);
            this.TimeWorking.Name = "TimeWorking";
            this.TimeWorking.Size = new System.Drawing.Size(0, 13);
            this.TimeWorking.TabIndex = 14;
            // 
            // CurrentFile
            // 
            this.CurrentFile.AutoSize = true;
            this.CurrentFile.Location = new System.Drawing.Point(446, 341);
            this.CurrentFile.Name = "CurrentFile";
            this.CurrentFile.Size = new System.Drawing.Size(0, 13);
            this.CurrentFile.TabIndex = 15;
            // 
            // FilesCount
            // 
            this.FilesCount.AutoSize = true;
            this.FilesCount.Location = new System.Drawing.Point(446, 374);
            this.FilesCount.Name = "FilesCount";
            this.FilesCount.Size = new System.Drawing.Size(0, 13);
            this.FilesCount.TabIndex = 16;
            // 
            // StartNewButton
            // 
            this.StartNewButton.Enabled = false;
            this.StartNewButton.Location = new System.Drawing.Point(354, 206);
            this.StartNewButton.Name = "StartNewButton";
            this.StartNewButton.Size = new System.Drawing.Size(123, 23);
            this.StartNewButton.TabIndex = 17;
            this.StartNewButton.Text = "Начать новый поиск";
            this.StartNewButton.UseVisualStyleBackColor = true;
            this.StartNewButton.Click += new System.EventHandler(this.StartNewButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.StartNewButton);
            this.Controls.Add(this.FilesCount);
            this.Controls.Add(this.CurrentFile);
            this.Controls.Add(this.TimeWorking);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.FilesFoundLabel);
            this.Controls.Add(this.FilesFound);
            this.Controls.Add(this.FilesCountLabel);
            this.Controls.Add(this.CurrentFileLabel);
            this.Controls.Add(this.TimeWorkingLabel);
            this.Controls.Add(this.TextToSearch);
            this.Controls.Add(this.TextToSearchLabel);
            this.Controls.Add(this.NamePattern);
            this.Controls.Add(this.NamePatternLabel);
            this.Controls.Add(this.StartDirectory);
            this.Controls.Add(this.StartDirectoryLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StartDirectoryLabel;
        private System.Windows.Forms.TextBox StartDirectory;
        private System.Windows.Forms.Label NamePatternLabel;
        private System.Windows.Forms.TextBox NamePattern;
        private System.Windows.Forms.Label TextToSearchLabel;
        private System.Windows.Forms.TextBox TextToSearch;
        private System.Windows.Forms.Label TimeWorkingLabel;
        private System.Windows.Forms.Label CurrentFileLabel;
        private System.Windows.Forms.Label FilesCountLabel;
        private System.Windows.Forms.TreeView FilesFound;
        private System.Windows.Forms.Label FilesFoundLabel;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button ContinueButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Label TimeWorking;
        private System.Windows.Forms.Label CurrentFile;
        private System.Windows.Forms.Label FilesCount;
        private System.Windows.Forms.Button StartNewButton;
    }
}

